<?php
/**
 * Created by PhpStorm.
 * User: Игорь
 * Date: 21.08.14
 * Time: 18:46
 */

function array_get($array,$key,$default = null){
    if (isset($array[$key])) return $array[$key];
    return $default;


}
/**
 * Class DOM
 */
class DOM
{
    /**
     * Plain html
     * @var
     */
    protected $html;
    /**
     * @var array
     */
    private $singleTag = ['meta', 'img', 'link', 'br', 'input', 'hr'];
    /**
     * If empty use all tags, else parse only this
     * @var array
     */
    protected $parseTag = [];
    /**
     * Ordered tag list
     * @var array
     */
    private $list = [];
    /**
     * All tags grouped by tagname
     * @var array
     */
    private $tags = [];
    /**
     * All tags grouped by ids
     * @var array
     */
    private $ids = [];
    /**
     * All tags grouped by classes
     * @var array
     */
    private $classes = [];
    /**
     * @var bool|null|string
     */
    private $enc;

    /**
     * @param string $html Plain html
     * @param null $enc encoding name
     * @param array $parseTag
     */
    function __construct($html,$enc = null,$parseTag = [])
    {
        $this->parseTag = $parseTag;
        if ($enc === null){
            $enc = mb_detect_encoding($html, ["euc-jp","utf-8"]);
        }
        if (strtolower($enc) !== "utf-8" && $enc !== false) {
            $this->html = iconv($enc, "utf-8", $html);
        } else {
            $this->html = $html;
        }
        $this->clear();
        $this->build();
        $this->enc = $enc;
    }

    /**
     * @return string
     */
    protected function getParseTagPattern(){
        if (count($this->parseTag)==0){
            return '[\w|\-]+';
        } else {
            return implode("|",$this->parseTag);
        }
    }

    /**
     * Build tags list
     */
    protected function build()
    {
//        $x1 = microtime(true);
        $stack = [];
        $tags = [];
        $classes = [];
        $ids = [];
        $parent = null;
        preg_match_all('/<(\/?)('.$this->getParseTagPattern().')(.*?)(\/?)>/us',$this->html,$matches, PREG_SET_ORDER | PREG_OFFSET_CAPTURE);
        $tree = [];
        foreach ($matches as $index=>&$match) {

            $tag = new DOMTag([
                'name' => &$match[2][0],
                'close' => $match[1][0] == '/',
                'closeSingle' => $match[4][0] == '/',
                'match' => &$match,
                'pos' => [$match[0][1],strlen($match[0][0])],
                'document' => $this,
                'children' => [],
            ]);

            if ($parent !== null) {
                $tag['parent'] = $parent;
                $parent->getItem()['childret'][]=$parent->children;
            }
            $tree[] = $tag;
            if (!$tag['close'] && !$this->isSingle($tag)) {
                $stack[] = $tag;
                $parent = $tag;


            }
            if ($tag['close']) {
                $openTag = array_pop($stack);
                if ($openTag) {
                    if ($openTag->name == $tag->name) {
                        $openTag['closeTag'] = $tag;
                        $tag['openTag'] = $openTag;
                    } else {
                        while ($openTag && $openTag->name !== $tag->name) {
//                            echo "Not closed " . $openTag->name . "\n";
                            $openTag = array_pop($stack);
                        }
                        if ($openTag->name == $tag->name) {
                            $openTag['closeTag'] = $tag;
                            $tag['openTag'] = $openTag;
                        }
                    }
                    $parent = $openTag->parent;

                }
            } else {
                if (!isset($tags[$tag->name])) {
                    $tags[$tag->name] = [];
                }

                $tags[$tag->name][] = $tag;
                $class = $tag->extractClass();
                while ($c = array_shift($class)) {
                    if (!isset($classes[$c])) {
                        $classes[$c] = [];
                    }
                    $classes[$c][] = $tag;
                }
                $id = $tag->extractId();
                if (!isset($ids[$id])) {
                    $ids[$id] = [];
                }
                $ids[$id][] = $tag;
            }

        }
        $this->list = & $tree;
        $this->classes = & $classes;
        $this->ids = & $ids;
        $this->tags = & $tags;
//        $x2 = microtime(true);
//        echo ($x2 - $x1) . " build\n";
    }

    /**
     *
     */
    protected function clear()
    {
        $this->html = preg_replace('/<!--.*?-->/us', '', $this->html);
//        echo(preg_last_error()." err\n");
        $this->html = preg_replace('/<!CDATA[.*?]]>/us', '', $this->html);
        $this->html = preg_replace('/<script.*?>.*?<\/script>/us', '', $this->html);
        $this->html = preg_replace('/<style.*?>.*?<\/style>/us', '', $this->html);
//        echo(preg_last_error()." err\n");
        $this->html = preg_replace('/(\n|\t|\r)+/us', ' ', $this->html);
//        echo(preg_last_error()." err\n");
//        file_put_contents(storage_path('/logs/clear1.html'), $this->html);
    }

    /**
     * Accept css selector
     * @param $selector
     * @return DOMTagCollection
     */
    public function find($selector)
    {
        /*
         * 1 - Если пробел, или мы последнее правило, то это самостоятельное правило
         * 2 - # - это ID, . - это класс
         * 3 - имя
         * 4 - атрибут
         * 5 - имя атрибута
         * 6 - значение атрибута
         * 7 - только у родителя
         */
        $rules = $this->parseSelector($selector);
        $rootGroup = $this->ruleGroupGenerator($rules)->current();
        $rules = array_slice($rules,count($rootGroup));
        $root = array_shift($rootGroup);
        if (array_get($root,2)=="#"){
            $found = array_get($this->ids, $root[3], []);
        } elseif (array_get($root,2)==".") {
            $found = array_get($this->classes, $root[3], []);
        } else {
            $found = array_get($this->tags, $root[3], []);
        }
        $matched = [];
        if (count($rootGroup)>0){
            foreach ($found as $item){
                if ($item->matchGroup($rootGroup)){
                    $matched[] = $item;
                }
            }
        } else {
            $matched = $found;
        }

        if (count($rules)>0){
            $matched = $this->match($matched, $rules);
        }
        return new DOMTagCollection($matched);
    }

    /**
     * @return mixed
     */
    public function html()
    {
        return $this->html;
    }

    /**
     * @param $item
     * @return bool
     */
    protected function isSingle($item)
    {
        return in_array($item['name'], $this->singleTag) || $item->closeSingle;
    }

    /**
     * @param $rules
     * @return \Generator
     */
    protected function ruleGroupGenerator($rules){
        $i = 1;
        $count = count($rules);
        $ruleGroup = [];
        $lastRule = $i == $count;
        foreach ($rules as $rule) {
            $ruleGroup[] = $rule;
            if (!$lastRule && !(array_get($rule, 1) === " ")) {
                $i++;
                $lastRule = $i == $count - 1;
                continue;
            }
            yield $ruleGroup;
             $ruleGroup = [];
            $i++;
            $lastRule = $i == $count - 1;
        }
    }

    /**
     * @param $found
     * @param $rules
     * @return array
     */
    protected function match($found, $rules)
    {
        $matched = [];
        foreach ($found as $item) {
            if (!$item->parent) {
                continue;
            }
            $match = true;
            $nextItem = $item;

            foreach ($this->ruleGroupGenerator($rules) as $ruleGroup) {
                if (!$nextItem->parent){
                    $match = false;
                    break;
                }
                $nextItem = $nextItem->parent->matchGroup($ruleGroup);
                if ($nextItem === false){
                    $match = false;
                    break;
                }
            }
            if ($match) {
                $matched[] = $item;
            }
        }
        return $matched;
    }

    /**
     * @param $selector
     * @return array
     */
    protected function parseSelector($selector)
    {
        $selector = preg_replace('/ >/','>',$selector);
        preg_match_all('/( )?([#|\.])?(\w+)(\[(\w+)="(.*?)"\])?([>])?/', $selector, $rules, PREG_SET_ORDER);
        $rules = array_reverse($rules);
        return $rules;
    }

    /**
     * @return bool|null|string
     */
    public function getEnc()
    {
        return $this->enc;
    }

    /**
     * @param $tree
     */
    protected function printTree($tree)
    {
        $tab = 0;
        $tabber = function ($tab) {
            while ($tab > 0) {
                echo " ";
                $tab--;
            }
        };
//        print_r($tree);
        foreach ($tree as $tag) {

            if (!$tag['close']) {
                $tabber($tab);
                echo $tag->match[0] . "\n";
                if (!$this->isSingle($tag)) $tab++;
            } else {
                $tab--;
                $tabber($tab);
                echo $tag->match[0] . "\n";
            }

        }
    }

}


