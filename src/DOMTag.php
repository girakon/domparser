<?php
/**
 * Created by PhpStorm.
 * User: Игорь
 * Date: 22.08.14
 * Time: 16:22
 */



class DOMTag implements \ArrayAccess
{
    private $item;

    public function __construct($item)
    {

        $this->item = $item;
    }

    /**
     * @return mixed
     */
    public function getItem()
    {
        return $this->item;
    }

    function __toString()
    {
        return (string)$this->item['pos'][0];
    }


    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Whether a offset exists
     * @link http://php.net/manual/en/arrayaccess.offsetexists.php
     * @param mixed $offset <p>
     * An offset to check for.
     * </p>
     * @return boolean true on success or false on failure.
     * </p>
     * <p>
     * The return value will be casted to boolean if non-boolean was returned.
     */
    public function offsetExists($offset)
    {
        return isset($this->item[$offset]);
    }

    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Offset to retrieve
     * @link http://php.net/manual/en/arrayaccess.offsetget.php
     * @param mixed $offset <p>
     * The offset to retrieve.
     * </p>
     * @return mixed Can return all value types.
     */
    public function offsetGet($offset)
    {
        return $this->item[$offset];
    }

    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Offset to set
     * @link http://php.net/manual/en/arrayaccess.offsetset.php
     * @param mixed $offset <p>
     * The offset to assign the value to.
     * </p>
     * @param mixed $value <p>
     * The value to set.
     * </p>
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        $this->item[$offset] = $value;
    }

    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Offset to unset
     * @link http://php.net/manual/en/arrayaccess.offsetunset.php
     * @param mixed $offset <p>
     * The offset to unset.
     * </p>
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->item[$offset]);
    }

    function __get($name)
    {
        if (isset($this->item[$name])){
            return $this->item[$name];
        }
        return null;
    }

    function __set($name, $value)
    {
        $this->item[$name] = $value;
    }

    public function html()
    {
        $start = $this->pos[0];
        if (isset($this['closeTag'])) {
            $len = $this->closeTag->pos[0] + $this->closeTag->pos[1] - $start;
        } else {
            $len = $this->pos[1];
        }
        return substr($this->document->html(), $start, $len);
    }

    public function innerHtml(){
        $start = $this->pos[0]+$this->pos[1];
        if (isset($this['closeTag'])) {
            $len = $this->closeTag->pos[0] - $start;
        } else {
            return "";
        }
        return substr($this->document->html(), $start, $len);
    }


    public function match($selector)
    {
        if (array_get($selector,2) == "#"){
            if ($this->matchId($selector)){
                return $this;
            }
        }
        if (array_get($selector,2)=='.'){
            if ($this->matchClass($selector)){
                return $this;
            }
        }
        if ($this->name == $selector[3]) {
            return $this;
        }

        if ($this->parent && array_get($selector,4)!==">") {
            return $this->parent->match($selector);
        }

        return false;
    }


    public function matchGroup($group){
        $match = true;
        $parent = true;
        foreach($group as $selector){
            /*
             * 1 - Если пробел, или мы последнее правило, то это самостоятельное правило
             * 2 - # - это ID, . - это класс
             * 3 - имя
             * 4 - атрибут
             * 5 - имя атрибута
             * 6 - значение атрибута
             * 7 - только у родителя
             */
            if (array_get($selector,7)==">"){
                $parent = false;
            }
            if (array_get($selector,2) == "#"){
                if ($this->matchId($selector)){
                    continue;

                }
            }
            if (array_get($selector,2)=='.'){
                if ($this->matchClass($selector)){
                    continue;
                }
            }
            if ($this->name == $selector[3]) {
                if (array_get($selector,4)){
                    if ( $this->matchAttr($selector)){
                        continue;
                    }
                } else {
                    continue;
                }
            }
            $match = false;
            break;
        }
        if ($match){
            return $this;
        } else {
            if ($parent && $this->parent){
                return $this->parent->matchGroup($group);
            }
        }
        return false;

    }

    /**
     * @param $selector
     * @return bool
     */
    protected function matchId($selector)
    {
        return $selector[3] == $this->id;
    }

    /**
     * @param $selector
     * @return bool
     */
    protected function matchClass($selector)
    {
        return in_array($selector[3], $this->class);
    }

    public function attr($name){
        if (preg_match('/'.$name.'="(.*?)"/ui', $this->item['match'][3][0], $match)) {
            return $match[1];
        }
        return null;
    }

    public function extractClass(){
        if ($this->class){
            return $this->class;
        }
        $this->class = explode(" ", $this->attr("class"));
        return $this->class;
    }

    public function extractId(){
        if ($this->id){
            return $this->id;
        }
        $this->id = $this->attr("id");
        return $this->id;
    }

    private function matchAttr($selector)
    {
        $name = $selector[5];
        $value = $selector[6];
        return $this->attr($name) === $value;
    }

    public function find($selector, $tags = []){
        if (!$this->dom){
            $this->dom = new DOM($this->html(),false,$tags);
        }
        return $this->dom->find($selector);

    }
}