<?php
require_once "vendor/autoload.php";
//Когда file_get_contents юзаешь, кодировка то euc-jp то utf-8, поэтому проще с дампа
$html = file_get_contents("dump1.html");
//file_put_contents("dump1.html",$html);
$x1 = microtime(true);
//Т.к. первый селектор #list01 > table > tr, нас не интересуют все остальные тэги, строим только по этим трем
$dom = new DOM($html,"utf-8",['div','table','tr']);
$x2 = microtime(true);
echo ($x2-$x1)."\n";
$x1 = microtime(true);
foreach ($dom->find("#list01 > table > tr")->each() as $item){
    $img = $item->find(".i img");
    if (count($img)<1){
        continue;
    } else {
        echo "Image: ".$img[0]->attr("src")."\n";
    }
    $title  = $item->find('h3 a');
    if (count($title)<1){
        echo "title not found\n";
    } else {
        echo "Title: ".$title[0]->attr('href')." ".$title[0]->innerHtml()."\n";
    }
    $sifwrp = $item->find('.sinfwrp a');
    if (count($sifwrp)<2){
        echo "user not found\n";
    } else {
        echo "User: ".$sifwrp[1]->attr('href')." ".$sifwrp[0]->innerHtml()."\n";
    }
    $price = $item->find(".pr2");
    if (count($price)<1){
        echo "price not found\n";
    } else {
        echo "Price: ".$price[0]->innerHtml()."\n";
    }



    echo "-----------\n";
}

$x2 = microtime(true);
echo ($x2-$x1)."\n";